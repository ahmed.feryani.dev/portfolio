import React from "react";
import { Meteor } from "meteor/meteor";
import { render } from "react-dom";
import Home from "../imports/ui/pages/Home/Home";
import "./main.scss";
//import Home from "../imports/ui/pages/Home/Home";

function App() {
  return (
    <div className="App">
      <Home></Home>
    </div>
  );
}

export default App;

Meteor.startup(() => {
  render(<App />, document.getElementById("react-target"));
});
