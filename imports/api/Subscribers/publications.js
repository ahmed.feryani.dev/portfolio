import { check } from "meteor/check";
import { Meteor } from "meteor/meteor";
import SubscribersCollection from ".";

Meteor.publish("Subscribers", function () {
  check(teamId, String);
  // Test against User, Rules, ...
  // use this.propName
  // ex: return Collection.find({user: this.userId})
  return SubscribersCollection.find({});
});
