import { Mongo } from "meteor/mongo";

const SubscribersCollection = new Mongo.Collection("Subscribers");

export default SubscribersCollection;
