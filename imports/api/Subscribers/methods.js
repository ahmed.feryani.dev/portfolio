import { check, Match } from "meteor/check";
import SubscribersCollection from ".";
let validator = require("email-validator");
Meteor.methods({
  "Subscribers.insert"(email) {
    check(
      email,
      Match.Where((email) => validator.validate(email))
    );
    SubscribersCollection.insert({
      email,
    });
  },
});
