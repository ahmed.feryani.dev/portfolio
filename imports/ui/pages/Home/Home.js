import SocialMedia from "../../components/SocialMedia/SocialMedia";
import Subscribe from "../../components/Subscribe/Subscribe";
import Logo from "../../ui/Logo/Logo";
//import photo from "../../../../public/group.jpg";
import "./style.scss";
import { motion } from "framer-motion";
import React from "react";
const Variants = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
    transition: {
      duration: 2,
    },
  },
};

const Home = () => {
  const img = "";
  return (
    <div className="home">
      <div className="home__action">
        <Logo></Logo>
        <div className="home__subscribe">
          <motion.div>
            <motion.p className="home__coming">It’s coming soon</motion.p>
            <motion.h1
              variants={Variants}
              initial="initial"
              animate="animate"
              className="home__heading"
            >
              My new website is on its way.
            </motion.h1>
            <p className="home__para">
              Sign up to be the first to know when It’s launched.
            </p>
            <Subscribe></Subscribe>
          </motion.div>
        </div>
        <SocialMedia></SocialMedia>
      </div>
      <div className="home__img">
        <img src="https://images.unsplash.com/photo-1610272612254-bcb3474421b8?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80" alt="" />
      </div>
    </div>
  );
};

export default Home;
