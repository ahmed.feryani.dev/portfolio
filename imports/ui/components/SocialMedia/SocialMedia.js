import React from "react";
import './style.scss';
import Youtube from '../../ui/Icons/Youtube/Youtube';
import LinkedIn from '../../ui/Icons/LinkedIn/LinkedIn';
import Instagram from '../../ui/Icons/Instagram/Instagram';
import Twitter from '../../ui/Icons/Twitter/Twitter';
import Facebook from '../../ui/Icons/Facebook/Facebook';



const SocialMedia = () => {
    const list = [
        <LinkedIn animate />,
        <Facebook animate />,
        <Instagram animate />,
        <Twitter animate />,
        <Youtube animate />,
    ];
    const renderList = (list) => {
        return (
            <ul className='socialMedia__list'>
                {(list.map((logo, index) => {
                    return <li key={index}>{logo}</li>
                }))}
            </ul>)
    }
    return (
        <div className='socialMedia'>
            {renderList(list)}
        </div>
    )
}

export default SocialMedia
