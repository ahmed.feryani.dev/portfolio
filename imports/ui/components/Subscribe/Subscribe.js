import React from "react";
import "./style.scss";
import Button from "../../ui/Button/Button";
import InputText from "../../ui/Input/InputText/InputText";
import { AnimatePresence, motion, useAnimation } from "framer-motion";
import { useEffect, useState } from "react";
import DotsWave from "../../ui/DotsWave/DotsWave";
import Confetti from "../../ui/Icons/Confetti/Confetti.js";
let validator = require("email-validator");
const btnVariants = {
  initial: {
    width: "17.5rem",
    borderRadius: "0 1.5rem 1.5rem 0",
    zIndex: 0,
  },
  animate: {
    width: "100%",
    zIndex: 1,
    borderRadius: "1.5rem 1.5rem 1.5rem 1.5rem",
  },
};

const fadeInVariants = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
  },
  exit: {
    opacity: 0,
    transition: {
      duration: 3,
    },
  },
};

const SubscribeLabel = () => {
  return (
    <motion.p
      exit={{
        opacity: 0,
      }}
      key={0}
    >
      Subscribe
    </motion.p>
  );
};
const CongratsLabel = (props) => {
  useEffect(() => {
    setTimeout(() => {
      props.controls.set("initial");
      props.setcongAnim(false);
      props.setlabelAnim(true);
    }, 3000);
  }, []);
  return (
    <motion.div
      className="chv"
      key={1}
      variants={fadeInVariants}
      initial="initial"
      animate="animate"
      exit={{
        display: "none",
      }}
    >
      <Confetti></Confetti>
      Congrats for being part of mbuiux community
    </motion.div>
  );
};

const Loader = (props) => {
  useEffect(() => {
    return () => {
      props.setcongAnim(true);
    };
  }, []);
  return (
    <motion.div
      key={2}
      exit={{
        opacity: 0,
        transition: {
          delay: 3,
        },
      }}
    >
      <DotsWave></DotsWave>
    </motion.div>
  );
};

const Subscribe = () => {
  const [labelAnim, setlabelAnim] = useState(true);
  const [dotsAnim, setdotsAnim] = useState(false);
  const [congAnim, setcongAnim] = useState(false);
  const [err, seterr] = useState("");

  const controls = useAnimation();
  const onSubmit = (e) => {
    const email = e.target.subscribe.value;
    const isValid = validator.validate(email);
    e.preventDefault();
    if (isValid) {
      setlabelAnim(false);
      controls
        .start("animate")
        .then(() => setdotsAnim(true))
        .then(() => {
          Meteor.call("Subscribers.insert", email);
          e.target.subscribe.value = "";
        })
        .then(() => setdotsAnim(false));
    }
  };

  return (
    <>
      <form className="subscribe" onSubmit={(e) => onSubmit(e)}>
        <motion.div className="subscribe__input">
          <InputText
            placeholder="Email address"
            name="subscribe"
            //type="email"
          ></InputText>
        </motion.div>

        <motion.div
          className="subscribe__btn"
          variants={btnVariants}
          initial="initial"
          animate={controls}
        >
          <Button type="btn--primary">
            <AnimatePresence exitBeforeEnter>
              {labelAnim && (
                <SubscribeLabel setdotsAnim={setdotsAnim}></SubscribeLabel>
              )}
              {dotsAnim && <Loader setcongAnim={setcongAnim}></Loader>}
              {congAnim && (
                <CongratsLabel
                  controls={controls}
                  setcongAnim={setcongAnim}
                  setlabelAnim={setlabelAnim}
                ></CongratsLabel>
              )}
            </AnimatePresence>
          </Button>
        </motion.div>
      </form>
    </>
  );
};

export default Subscribe;
