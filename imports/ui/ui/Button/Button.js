import React from "react";
import "./style.scss";
import cx from "classnames";
import { motion } from "framer-motion";

const Varients = {
  initial: {
    background: "hsl(232, 57%, 56%)"
  },
  hover: {
    background: "hsl(232, 54%, 49%)"
  },
  tap: {
    background: "hsl(232, 54%, 41%)"
  }
}

const Button = (props) => {
  return (
    <motion.button
      variants={Varients}
      initial='initial'
      whileHover='hover'
      whileTap='tap'
      onClick={props.action}
      className={cx("btn", "chv", props.type)}
    >
      {props.children}
    </motion.button>
  );
};

export default Button;
