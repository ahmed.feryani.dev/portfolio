import React from "react";
import { motion } from 'framer-motion'

const Variants = {
    hover: {
        fill: '#5062d0',
    }
}


const Youtube = (props) => {
    const variants = props.animate ? Variants : {}
    return (
        <motion.svg
            style={{
                cursor: 'pointer'
            }}
            variants={variants}
            whileHover='hover'
            fill="#CBD2E4" xmlns="http://www.w3.org/2000/svg" width="34" height="24" viewBox="0 0 34 24">
            <path d="M243.568 17.169l8.877-5.142-8.877-5.136V17.17zm4.388 6.825c-3.482 0-6.964.004-10.445-.001-3.178-.005-5.74-2.06-6.382-5.132-.086-.41-.118-.84-.12-1.26-.01-3.715-.01-7.43-.007-11.145.003-3.222 2.229-5.831 5.434-6.365.406-.068.825-.088 1.238-.088 6.88-.004 13.761-.006 20.642.004 3.167.004 5.675 1.852 6.451 4.754.133.496.213 1.021.215 1.533.02 3.797.023 7.594.01 11.39-.01 3.48-2.87 6.3-6.384 6.31-3.55.011-7.101.003-10.652.003v-.003z" transform="translate(-312 -900) translate(76 55) translate(5 845)" />
        </motion.svg>
    )
}

export default Youtube
