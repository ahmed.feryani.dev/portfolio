import React from "react";
import { motion } from "framer-motion"
const Variants = {
    hover: {
        fill: '#5062d0',
    }
}

const LinkedIn = (props) => {
    const variants = props.animate ? Variants : {}
    return (
        <motion.svg
            style={{
                cursor: 'pointer'
            }}
            variants={variants}
            whileHover='hover'
            fill="#CBD2E4" xmlns="http://www.w3.org/2000/svg" width="26" height="25" viewBox="0 0 26 25">
            <path d="M23.688.257H2.339c-.994 0-1.8.79-1.8 1.766v20.954c0 .975.806 1.766 1.8 1.766h21.349c.994 0 1.8-.79 1.8-1.766V2.023c0-.975-.806-1.766-1.8-1.766zm-14.3 18.508H6.352v-8.97h3.038v8.97zM7.87 8.57h-.02c-1.02 0-1.679-.69-1.679-1.55 0-.88.68-1.55 1.72-1.55 1.038 0 1.678.67 1.698 1.55 0 .86-.66 1.55-1.72 1.55zm12.473 10.195h-3.038v-4.8c0-1.205-.44-2.028-1.539-2.028-.839 0-1.339.555-1.558 1.09-.08.192-.1.46-.1.728v5.01H11.07s.04-8.129 0-8.97h3.038v1.27c.403-.612 1.126-1.481 2.737-1.481 2 0 3.498 1.282 3.498 4.037v5.144z" transform="translate(-81 -900) translate(76 55) translate(5 845)" />
        </motion.svg>

    )
}

export default LinkedIn


