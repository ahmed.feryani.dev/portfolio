import React from "react";
import { motion } from "framer-motion"

const Variants = {
    hover: {
        fill: '#5062d0',
    }
}


const Facebook = (props) => {
    const variants = props.animate ? Variants : {};
    return (
        <motion.svg
            style={{
                cursor: 'pointer'
            }}
            variants={variants}
            whileHover='hover'
            fill="#CBD2E4"
            xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">

            <path d="M21.631 0H3.37C1.509 0 0 1.508 0 3.369V21.63C0 23.491 1.508 25 3.369 25h9.007l.015-8.934h-2.32c-.303 0-.547-.244-.549-.545l-.01-2.88c-.002-.303.244-.55.547-.55h2.317V9.31c0-3.23 1.972-4.987 4.852-4.987h2.364c.303 0 .548.245.548.547v2.428c0 .303-.245.548-.548.548h-1.45c-1.567 0-1.87.745-1.87 1.838v2.408h3.442c.328 0 .583.287.544.613l-.341 2.88c-.033.275-.267.482-.544.482h-3.086L16.272 25h5.36C23.491 25 25 23.492 25 21.631V3.37C25 1.509 23.492 0 21.631 0z" transform="translate(-138 -900) translate(76 55) translate(5 845) translate(57)" />

        </motion.svg>
    )
}

export default Facebook
