import React from "react";
import { motion } from "framer-motion"
const Variants = {
    hover: {
        fill: '#5062d0',
    }
}

const Twitter = (props) => {
    const variants = props.animate ? Variants : {}
    return (
        <motion.svg
            style={{
                cursor: 'pointer'
            }}
            variants={variants}
            whileHover='hover'
            fill="#CBD2E4"
            xmlns="http://www.w3.org/2000/svg" width="31" height="25" viewBox="0 0 31 25">
            <path d="M200.368 2.9c-1.13.49-2.333.817-3.588.975 1.291-.765 2.276-1.967 2.74-3.415-1.204.712-2.533 1.215-3.949 1.495C194.43.748 192.801 0 191.024 0c-3.447 0-6.222 2.777-6.222 6.18 0 .49.042.962.145 1.41-5.176-.25-9.756-2.713-12.833-6.463-.537.925-.852 1.983-.852 3.123 0 2.14 1.11 4.037 2.766 5.135-1-.019-1.982-.307-2.813-.76v.067c0 3.003 2.158 5.497 4.988 6.072-.507.137-1.06.203-1.633.203-.398 0-.8-.022-1.178-.105.806 2.447 3.095 4.246 5.817 4.304-2.118 1.645-4.808 2.636-7.72 2.636-.51 0-1-.023-1.489-.085 2.758 1.765 6.026 2.773 9.55 2.773 11.457 0 17.72-9.42 17.72-17.584 0-.273-.01-.537-.023-.799 1.236-.87 2.274-1.957 3.12-3.208z" transform="translate(-251 -900) translate(76 55) translate(5 845)"
            />
        </motion.svg>
    )
}

export default Twitter
