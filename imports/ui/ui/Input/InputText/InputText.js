import React from "react";
import { motion } from "framer-motion";
import "./style.scss";

const InputText = (props) => {
  const { placeholder, name, type = "text" } = props;

  return (
    <div className="inputText">
      <input
        className="inputText__input"
        type={type}
        placeholder={placeholder}
        name={name}
      />
    </div>
  );
};

export default InputText;
